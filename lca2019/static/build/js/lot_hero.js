function hero() {
  var timeouts = new Array(3);
  let green = "#0F7C11";
  let radius = 7;
  let windowWidth;
  let breakPoint = 768;
  
  var loeWrapper = document.getElementById("loe-wrapper");
  var loeOf = document.getElementById("of");
  var canvas = document.getElementById("hero");
  var bottomWrapper = document.getElementById("bottom-wrapper");
  var registerLink = document.getElementById("register-link");
  var thingsHeading = document.getElementById("things-heading");
  
  var ctx = canvas.getContext("2d");
  var canvasPadding = 10;
  
  function positionRegisterLink() {
    registerLink.style.position = 'static';
    registerLink.style.float = 'none'; 
    var offset = registerLink.offsetTop - loeOf.offsetTop - 0.25 * loeOf.offsetHeight;
    if (window.innerWidth >= breakPoint) {
      registerLink.style.top = -1 * offset + 'px';
      registerLink.style.position = 'relative';
      registerLink.style.float = 'right'; 
    }
  }
  
  function getCanvasPos() {
    var canvasRatio = window.innerWidth < 576 ? 1.1 : 2.5;
    var width = loeWrapper.offsetWidth - loeOf.offsetWidth - (2 * canvasPadding);
    var minHeight = 180;
    if (window.innerWidth >= breakPoint) {
      width -= registerLink.offsetWidth;
      minHeight = 260;
    }
    
    return {
      width: width,
      height: Math.max(width / canvasRatio, minHeight),
      left: loeOf.offsetWidth + canvasPadding,
      top: -1.25 * loeOf.offsetHeight,
    }
  }
  
  function sizeCanvas(pos) {
    var c = canvas;
    c.width = pos.width;
    c.height = pos.height;
    c.style.width = pos.width + 'px';
    c.style.height = pos.height + 'px';
    c.style.position = 'relative';
    c.style.left = pos.left + 'px';
    c.style.top = pos.top + 'px';
    c.style.marginBottom = c.style.top;
  }
  
  function positionThingsHeading() {
    thingsHeading.style.position = 'relative';
    thingsHeading.style.display = 'inline-block';
    var left = canvas.offsetWidth - thingsHeading.offsetWidth
    if(window.innerWidth >= breakPoint) {
      left += registerLink.offsetWidth;
    }
    thingsHeading.style.left = left + 'px';
    thingsHeading.style.top = -0.7 * thingsHeading.offsetHeight + 'px';
    thingsHeading.style.marginBottom = thingsHeading.style.top;
  }
  
  function positionDate() {
    bottomWrapper.style.position = 'static';
    bottomWrapper.style.marginBottom = 0;
    var offs = -0.7 * thingsHeading.offsetHeight;
    offs -= bottomWrapper.offsetHeight * 0.8;
    if (window.innerWidth >= 576) {
      bottomWrapper.style.position = 'relative';
      bottomWrapper.style.top = offs + 'px';
      bottomWrapper.style.marginBottom = offs + 20 + 'px';
    }
  }
  
  function drawLine(ind, context, startX, startY, endX, endY, color) {
    let dx = startX - endX;
    let dy = startY - endY;
    
    let theta = Math.atan2(dy, dx);
    var x = startX - 15 * Math.cos(theta);
    var y = startY - 15 * Math.sin(theta);
    
    endX = endX + 30 * Math.cos(theta);
    endY = endY + 30 * Math.sin(theta)
    let delay = 8;
    let increments = 2080 / delay;
    let xAdjust = (endX - startX) / increments;
    let yAdjust = (endY - startY) / increments;
    let counter = 1;
    
    function line() {
      context.beginPath();
      context.lineWidth = 1.9;
      context.moveTo(x, y);
      x = x + xAdjust;
      y = y + yAdjust;
      context.lineTo(x, y);
      context.strokeStyle = color;
      context.stroke();
      counter++;
      if (counter <= increments) {
        timeouts[ind] = window.setTimeout(line, delay);
      }
    };
    timeouts[ind] = window.setTimeout(line, delay);
  }
  
  function drawCircle(context, x, y, radius, color) {
    context.beginPath();
    context.fillStyle = color;
    context.arc(x, y, radius, 0, 2 * Math.PI);
    context.fill();
  }
  
  function windowResize() {
    if(window.innerWidth === windowWidth) {
      // Catch mobile scroll event, as this can hide/unhide the address
      // bar and fire a resize event.
      return;
    } else {
      windowWidth = window.innerWidth;
    }
    for(var i = 0; i < 3; i++) {
      window.clearTimeout(timeouts[i]);
    }
    var canvasPos = getCanvasPos();
    sizeCanvas(canvasPos)
    positionThingsHeading();
    positionDate();
    
    function getLowestPoint() {
      var pos = thingsHeading.offsetLeft - canvas.offsetLeft - 20;
      if(pos < 10) {
        pos = thingsHeading.offsetLeft + 0.5 * thingsHeading.offsetWidth;
      }
      return pos
    }
    
    let points = [
      [10, 10],
      [canvasPos.width - 10, 10],
      [
        getLowestPoint(),
        canvasPos.height -10
      ]
    ];
    
    positionRegisterLink();
    
    drawCircle(ctx, points[0][0], points[0][1], radius, green);
    drawCircle(ctx, points[1][0], points[1][1], radius, green);
    drawCircle(ctx, points[2][0], points[2][1], radius, green);
    
    drawLine(0, ctx, points[0][0], points[0][1], points[2][0], points[2][1], green);
    drawLine(1, ctx, points[2][0], points[2][1], points[1][0], points[1][1], green);
    drawLine(2, ctx, points[1][0], points[1][1], points[0][0], points[0][1], green);
    
  }
  
  windowResize();
  window.addEventListener('resize', windowResize, false);
}

hero();
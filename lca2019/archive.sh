#!/bin/sh

wget --no-host-directories --page-requisites --mirror --force-directories --adjust-extension --convert-links --span-hosts --domains=lca2019.linux.org.au,cloudflare.com,code.jquery.com,fonts.googleapis.com,fonts.gstatic.com,unpkg.com -e robots=off https://lca2019.linux.org.au/

find -name '*.html' | xargs sed -i 's%<script async src="https://www.googletagmanager.com/gtag/js?id=UA-63600300-1"></script>%%'

sed -i 's/integrity="sha512-.*=="//' index.html

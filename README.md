# archives

Scripts (and the output of) to archive dynamic websites. We want local copies of all the bits and pieces that give the website it's look and feel (html, css, fonts, js) but nothing that we don't have the copyright for.
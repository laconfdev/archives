#!/bin/sh

wget --no-clobber --no-host-directories --page-requisites --mirror --force-directories --adjust-extension --convert-links --span-hosts --domains=lca2018.org,lca2018.linux.org.au,cdnjs.cloudflare.com,ajax.googleapis.com,cdn.pride.codes,cdn.jsdelivr.net -e robots=off https://lca2018.org/ https://lca2018.org/wiki

for f in $( find -name '*.html' )
do
  dir=$( echo $f | sed 's%/[a-z0-9.]*%/..%g' | sed 's%\./../%%' )
  image=${dir}/media-2b3cf12/img/LCA18_nodate.svg
  sed -i "s%https://linux.conf.au/media/img/LCA18_nodate.svg%${image}%" $f
done
find -name '*.html' | xargs sed -i -e 's%href="https://wiki.linux.conf.au/"% href="/wiki/"%g' -e 's%href="https://wiki.linux.conf.au/favicon.ico"%href="/favicon.ico"%'

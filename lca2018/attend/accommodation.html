<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <title>linux.conf.au 2018 - Sydney 22-26 January 2018</title>
  <meta name="description" content="linux.conf.au 2018 accommodation">
  <meta name="author" content="LCA2018">

  <meta property="og:type" content="website" />
  <meta property="og:title" content="linux.conf.au 2018 - Sydney 22-26 January 2018" />
  <meta property="og:image" content="https://linux.conf.au/media-2b3cf12/img/LCA18_full_large.png" />
  <meta property="og:image:alt" content="linux.conf.au 2018" />

  <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="../media-2b3cf12/img/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../media-2b3cf12/img/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../media-2b3cf12/img/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../media-2b3cf12/img/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="../media-2b3cf12/img/apple-touch-icon-60x60.png" />
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="../media-2b3cf12/img/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="../media-2b3cf12/img/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="../media-2b3cf12/img/apple-touch-icon-152x152.png" />
  <link rel="icon" type="image/png" href="../media-2b3cf12/img/favicon-196x196.png" sizes="196x196" />
  <link rel="icon" type="image/png" href="../media-2b3cf12/img/favicon-96x96.png" sizes="96x96" />
  <link rel="icon" type="image/png" href="../media-2b3cf12/img/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="../media-2b3cf12/img/favicon-16x16.png" sizes="16x16" />
  <link rel="icon" type="image/png" href="../media-2b3cf12/img/favicon-128.png" sizes="128x128" />
  <meta name="application-name" content="linux.conf.au 2018"/>
  <meta name="msapplication-TileColor" content="#9e9e9e" />
  <meta name="msapplication-TileImage" content=/media-2b3cf12/mstile-144x144.png />
  <meta name="msapplication-square70x70logo" content=/media-2b3cf12/mstile-70x70.png />
  <meta name="msapplication-square150x150logo" content=/media-2b3cf12/mstiyle-150x150.png />
  <meta name="msapplication-wide310x150logo" content=/media-2b3cf12/mstile-310x150.png />
  <meta name="msapplication-square310x310logo" content=/media-2b3cf12/mstile-310x310.png />
  
    <link rel="stylesheet" href="../media-2b3cf12/css/bar_body.css">
  <link rel="stylesheet" href="../media-2b3cf12/css/bootstrap.min.css">
  <link rel="stylesheet" href="../media-2b3cf12/css/custom.css">
  
    <!-- All JavaScript at the bottom, except for Modernizr which
        enables HTML5 elements & feature detects -->
  <script src="../media-2b3cf12/js/libs/modernizr-1.7.min.js"></script>
    </head>
<body id="index">
    <div class="container">
    <header>
                  <nav class="navbar navbar-expand-lg navbar-light" role="navigation">
  <a class="navbar-brand logo" href="../index.html">
    <img src="../media-2b3cf12/img/LCA18_nodate.svg" height="80" alt="linux.conf.au 2018" />
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle page-scroll" id="navbarDropdownMenuLinkAbout"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span>About</span>
  </a>
  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLinkAbout">
    <a class="dropdown-item"href="../about.html">
      linux.conf.au 2018
    </a><a class="dropdown-item"href="../about/contact.html">
      Contact
    </a><a class="dropdown-item"href="../about/venue.html">
      Venue
    </a><a class="dropdown-item"href="../about/codeclub.html">
      Code Club Australia
    </a><a class="dropdown-item" href="/wiki/">
      Wiki
    </a></div>
</li><li class="nav-item">
  <a class="nav-link page-scroll"href="../news.html">
    <span>News</span>
  </a>
</li><li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle page-scroll" id="navbarDropdownMenuLinkAttend"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span>Attend</span>
  </a>
  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLinkAttend">
    <a class="dropdown-item"href="https://rego.lca2018.org/">
      Dashboard
    </a><a class="dropdown-item"href="../attend.html">
      Tickets & Prices
    </a><a class="dropdown-item"href="accommodation.html">
      Accommodation
    </a><a class="dropdown-item"href="business-case.html">
      Business Case
    </a><a class="dropdown-item"href="shirts.html">
      Shirts
    </a><a class="dropdown-item"href="code-of-conduct.html">
      Code of Conduct
    </a><a class="dropdown-item"href="terms-and-conditions.html">
      Terms & Conditions
    </a><a class="dropdown-item"href="volunteer.html">
      Volunteer
    </a><a class="dropdown-item" href="/wiki/">
      Attendee Wiki
    </a><a class="dropdown-item"href="faq.html">
      FAQ
    </a></div>
</li><li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle page-scroll" id="navbarDropdownMenuLinkProgramme"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span>Programme</span>
  </a>
  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLinkProgramme">
    <a class="dropdown-item"href="../programme.html">
      Overview
    </a><a class="dropdown-item"href="../schedule.html">
      Conference Schedule
    </a><a class="dropdown-item"href="../programme/miniconfs.html">
      Miniconfs
    </a><a class="dropdown-item"href="../proposals.html">
      Proposals
    </a></div>
</li><li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle page-scroll" id="navbarDropdownMenuLinkSponsors"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span>Sponsors</span>
  </a>
  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLinkSponsors">
    <a class="dropdown-item"href="../sponsors.html">
      Our Sponsors
    </a><a class="dropdown-item"href="../sponsors/why-sponsor.html">
      Why Sponsor
    </a></div>
</li>    </ul>
  </div>
</nav>
          </header>
        <section class="content"><h1 class="heading-motif building1">Accommodation Options</h1><p>There are many hotels offering rooms for a range of budgets in Sydney.
Please note that January 26 is a public holiday, which may lead to increased rates for that night at some locations.</p>
<p>linux.conf.au 2018 has also arranged discounted accommodation with surrounding providers.
Full details are available to delegates who have registered for an account on the website.</p>
<p><em>NOTE</em>: Most room blocks have now been finalized with the hotels. If you are
still looking for accommodation we suggest you still reach out to the hotel and
see if there are rooms available and whether you can get the group rate, but it
will be subject to general availability.</p>
<h2 id="accessible-rooms">Accessible rooms</h2>
<p>A limited number of accessible rooms are available at the "Glebe
Space", and at the Mercure. Please contact the properties using the
links below to enquire about availability for these.</p>
<h2 id="value-accommodation-at-freespace">Value accommodation at FreeSpace</h2>
<p>We are pleased to offer value accommodation through
<a href="http://www.freespace.sydney">FreeSpace</a>, at their
<a href="https://goo.gl/maps/ACctZuGYdTm">Glebe</a> and
<a href="https://goo.gl/maps/VsYVaFPti9x">Sydney</a> spaces.</p>
<p>A range of rooms are available exclusively to attendees of
linux.conf.au 2018 for a limited time.</p>
<ul>
<li>$100/night - Double room at Sydney Space</li>
<li>$110/night - Double ensuite room at Glebe Space</li>
<li>$130/night - Twin/Triple ensuite room at Glebe Space</li>
</ul>
<p>These rooms are individual dorm style rooms and have shared bathrooms and
common areas.</p>
<p>All rooms include complimentary wireless internet access.
Pillows, linens, towels and toiletries are provided.</p>
<p>These buildings are next door to each other, near the Parramatta Road entrance
to Sydney University.</p>
<p>It's about a 30 minute walk to the UTS venue, or you can take a 10-minute bus
trip (using Sydney's public transport). These buses are very frequent, so we
will not run shuttles - getting to and from the venue is up to attendees.</p>
<p>Accommodation bookings have now closed, however we did release some rooms back
to the venue so they may be available. Please email <a href="http://lca2018.linux.org.au/attend/accommodation/groups@fspaceproperty.com">groups@fspaceproperty.com</a>
with LCA2018 in the title to see if there is any availability left.</p>
<h2 id="value-accommodation-at-song-hotel">Value accommodation at Song Hotel</h2>
<p>We also have value accommodation available through <a href="https://songhotels.com.au/song-hotel-redfern/">Song Hotel
Redfern</a>
<a href="https://goo.gl/maps/Btm1F52oGQE2">nearby</a>.</p>
<p>Double and twin rooms are available for $135/night.</p>
<p>These rooms are more traditional hotel rooms, each with an ensuite, a TV, a
fridge, etc. The Song Hotel also provides a complimentary "Light
Continental" breakfast.</p>
<p>This hotel is a 10-15 minute walk from the venue (crossing one major road).
Attendees will need to make their own arrangements in getting to and from the
venue each day.</p>
<p>Accommodation bookings via the dashboard have now closed, however you can still
book via <a href="https://songhotels.com.au/song-hotel-redfern/">the website</a>. These
rooms are open to the general public and are therefore based on availability. Make
sure you select the Redfern property not the Sydney property. You may be able to
get the discounted conference rate if you enquire directly with them.</p>
<h2 id="discounts-at-nearby-hotels">Discounts at nearby hotels</h2>
<p>A number of hotels close to the venue are offering discounted rates
for attendees of linux.conf.au 2018.</p>
<p>These rooms are subject to availability and are subject to change.</p>
<h3 id="vulcan-hotel">Vulcan Hotel</h3>
<ul>
<li>Our code gives 10% off market rates, subject to availability, for stays checking in on or after Sun 21 Jan
and checking out on or before Friday 27th Jan.</li>
</ul>
<div class="my-3">
  <a class="btn btn-primary btn-md" href="https://rego.linux.conf.au/accommodation/hotels/">Discount codes and instructions</a>
</div>

<h3 id="accor-group">Accor Group</h3>
<p>We've arranged a group booking code at 3 hotels run by the Accor Group in the vicinity of the conference.</p>
<p>These rates may not be the cheapest available rates at the hotels, so
you may wish to compare with prices available elsewhere.</p>
<p>However, our rates do include complimentary wifi for up to 5 devices
per room. These rates permit cancellation up to 30 days prior to
check-in (a cancellation fee of the first night's accommodation will
apply) and are transferable (you can change the name on the booking
if you aren't coming but can find someone else to take the
room). Cheaper rates may not allow this flexibility.</p>
<h4 id="payment-terms">Payment terms</h4>
<p>A non-refundable deposit equal to one night's accommodation will be
charged to guests' credit card at the time of reservation. At 29 days
prior to arrival, the guest's individual credit card will be charged
for all remaining reserved nights.</p>
<p>Should an individual cancel/reduce his/her reservation less than 29
days prior to arrival the above-mentioned payments will be forfeited
as a cancellation charge.</p>
<p>Should an individual fail to arrive at the hotel they will be
considered a No Show and they will be charged all nights booked.</p>
<h4 id="rates">Rates</h4>
<p>These rates apply from Sunday night (21 Jan 2018) through Friday night
(26 Jan 2018), and are subject to availability. These rates cannot be
guaranteed after December 21 and may run out before depending on
demand. Market rates apply for other nights.</p>
<p>These rates are only available for phone or email bookings and will
not be presented on the hotel websites.</p>
<p>Discounted parking is available for $25/day at each location.</p>
<p>Breakfast may be added at the discounted price of $20 per person per day.</p>
<ul>
<li>Novotel Sydney Central: $249/night; <a href="http://www.novotelsydneycentral.com.au/en/accommodation/standard-rooms.html">Standard Room</a></li>
<li>Mercure Sydney: $229/night; <a href="http://www.mercuresydney.com.au/accommodation_viewItem_581-en.html">Standard Room</a></li>
<li>Ibis World Square: $199/night; <a href="http://www.ibissydneyworldsquare.com.au/en/accommodation/standard-twin-room.html">Standard Twin</a> or <a href="http://www.ibissydneyworldsquare.com.au/en/accommodation/standard-double-room.html">Standard Double</a></li>
</ul>
<h4 id="bookings">Bookings</h4>
<div class="my-3">
  <a class="btn btn-primary btn-md" href="https://rego.linux.conf.au/accommodation/hotels/">Discount codes and instructions</a>
</div>
    </section>
    
    <div class="row mt-5 mb-2">
      <div class="col">
        <div class="card">
          <h3 class="card-header">Sponsors</h3>
          <div class="card-body">
            <div class="row align-items-center">
              <div class="col-md-2 col-lg-4 mr-auto">
                <p class="penguin-thanks">We thank our sponsors for their generous contribution to linux.conf.au 2018.</p>
                <a href="../sponsors.html" class="btn btn-outline-primary mb-3 mb-md-0">View all sponsors</a>
              </div>
              <div class="col col-md-4">
                <h4 class="card-title">Emperor Penguin</h4>
                <div class="penguin-logo penguin-emperor my-5">
                  <a href="https://www.ibm.com" class="d-block">
                    <img class="img-fluid mh-25" src="../media-2b3cf12/img/sponsors/ibm-logo.jpg" />
                  </a>
                </div>
              </div>
              <div class="col col-md-4">
                <h4 class="card-title">Venue</h4>
                <div class="penguin-logo penguin-emperor my-5">
                  <a href="https://www.uts.edu.au" class="d-block">
                    <img class="img-fluid mh-25" src="../media-2b3cf12/img/sponsors/uts-logo.svg" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <footer class="row">
      <div class="col ml-auto">
        <nav class="footer my-2" role="navigation">
          <a href="accommodation.html#">Back to top</a><br>
          &copy; 2017 linux.conf.au and <a href="http://linux.org.au/">Linux Australia</a><br>
          Linux is a registered trademark of Linus Torvalds<br>
          <a href="https://gitlab.com/LCA2018/lca2018-static">Website Source</a> | <a href="https://gitlab.com/LCA2018/lca2018-static/issues">Report an issue</a> | <a href="../attributions.html">Acknowledgements</a><br/>
        </nav>
      </div>
    </footer>
  </div> <!--! end of #container -->
      <!-- Javascript at the bottom for fast page loading -->
    <!-- Grab Google CDN's jQuery, with a proþtocol relative URL; fall back to local if necessary -->
  <script src="../media-2b3cf12/js/jquery-3.2.1.slim.min.js"></script>
  <script>window.jQuery || document.write('<script src="../media-2b3cf12/js/libs/jquery-3.2.1.min.js">\x3C/script>')</script>
    <script src="../media-2b3cf12/js/libs/popper.min.js"></script>
  <script src="../media-2b3cf12/js/libs/bootstrap.min.js"></script>

    
    <!-- asynchronous google analytics: mathiasbynens.be/notes/async-analytics-snippet
       change the UA-XXXXX-X to be your site's ID -->
<script>
    var _gaq = [['_setAccount', 'UA-63600300-1'], ['_trackPageview']];
    (function(d, t) {
    var g = d.createElement(t),
        s = d.getElementsByTagName(t)[0];
    g.async = true;
    g.src = ('https:' == location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g, s);
    })(document, 'script');
</script>
  
  </body>
</html>